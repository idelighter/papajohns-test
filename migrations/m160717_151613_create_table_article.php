<?php

use yii\db\Migration;
use app\models\Article;

class m160717_151613_create_table_article extends Migration
{
    public function safeUp()
    {
        $this->createTable(Article::tableName(), [
            'id' => 'pk',
            'title' => 'string',
            'text' => 'text',
            'created_at' => 'datetime NOT NULL',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');
    }

    public function safeDown()
    {
        $this->dropTable(Article::tableName());
    }
}
