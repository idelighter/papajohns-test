<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\Article;
use yii\helpers\Url;
use yii\helpers\StringHelper;

/* @var $model Article */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div id="articleModal"
         class="modal fade bs-example-modal-lg"
         tabindex="-1"
         role="dialog"
         aria-labelledby="createArticleModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Create Article</h4>
                </div>
                <div class="modal-body">
                    <?php $form = ActiveForm::begin([
                        'action' => Url::to(['article/create']),
                    ]); ?>
                    <?= $form->field($model, 'title')->textInput() ?>
                    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="modal-footer">
                    <button id="articleSave" type="button" class="btn btn-success">Save</button>
                </div>
            </div>
        </div>
    </div>

<?php $pjax = Pjax::begin(); ?>

    <div class="row">
        <div class="col-xs-9">
            <?php if ($dataProvider->count): ?>
                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->pagination,
                    'firstPageLabel' => "&laquo;&laquo;",
                    'lastPageLabel' => "&raquo;&raquo;",
                ]) ?>
            <?php else: ?>
                <p class="lead" style="margin: 20px 0;">No articles yet</p>
            <?php endif; ?>
        </div>
        <div class="col-xs-3">
            <button type="button"
                    class="btn btn-success pull-right"
                    data-toggle="modal"
                    data-target=".bs-example-modal-lg"
                    style="margin: 20px 0;">Create Article
            </button>
        </div>
    </div>

<?php /** @var Article $data */
foreach ($dataProvider->models as $data): ?>
    <div class="panel panel-success">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-9">
                    <?= $data->title ? Html::encode($data->title) : StringHelper::truncateWords($data, 10) ?>
                </div>
                <div class="col-xs-3">
                    <div class="pull-right">
                        <?= Yii::$app->formatter->asDatetime($data->created_at) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <?= StringHelper::truncateWords($data, 10) ?>
            <div class="row">
                <div class="col-xs-9">
                    <a href="<?= Url::to(['article/view', 'id' => $data->id]) ?>"
                       data-pjax="0"
                       role="button">Read more...</a>
                </div>
                <div class="col-xs-3">
                    <div class="pull-right">
                        <?= (count($data->comments) ?: 'No') . ' comments' ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<?php Pjax::end(); ?>

<?php $js = <<< JS
$(document).ready(function () {
    var form = $('#{$form->id}');
    var pjaxId = '{$pjax->id}';
    var btn = $('#articleSave');
    var modal = $('#articleModal');

    btn.click(function () {
        $.ajax({
            method: 'POST',
            url: form.attr('action'),
            data: form.serialize(),
            beforeSend: function () {
                btn.prop('disabled', true);
            },
            complete: function () {
                btn.prop('disabled', false);
            }
        })
        .done(function (data, textStatus) {
            if (data.status == textStatus) { // success
                $.pjax.reload('#' + pjaxId);
            }
        })
        .always(function () {
            modal.modal('hide');
        });
    });
});
JS;

$this->registerJs($js);
