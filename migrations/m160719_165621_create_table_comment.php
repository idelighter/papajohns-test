<?php

use yii\db\Migration;
use app\models\Article;
use app\models\Comment;

class m160719_165621_create_table_comment extends Migration
{
    public function safeUp()
    {
        $this->createTable(Comment::tableName(), [
            'id' => 'pk',
            'article_id' => 'integer NOT NULL',
            'comment_id' => 'integer',
            'name' => 'string',
            'email' => 'string',
            'text' => 'text',
            'rating' => 'bigint NOT NULL DEFAULT 0',
            'created_at' => 'datetime NOT NULL',
        ], 'ENGINE=InnoDB DEFAULT CHARSET=utf8');

        $this->addForeignKey('fk-comment-article_id',
            Comment::tableName(),
            'article_id',
            Article::tableName(),
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable(Comment::tableName());
    }
}
