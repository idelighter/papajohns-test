<?php

namespace app\models;

use Yii;
use yii\helpers\Html;
use yii\db\Expression;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $article_id
 * @property integer $comment_id
 * @property string $name
 * @property string $email
 * @property string $text
 * @property integer $rating
 * @property string $created_at
 *
 * @property Article $article
 * @property Comment[] $comments
 */
class Comment extends \yii\db\ActiveRecord
{
    public $captcha;

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article_id', 'name', 'email', 'text', 'captcha'], 'required'],
            [['article_id', 'comment_id', 'rating'], 'integer'],
            [['text'], 'string'],
            [['name', 'email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['captcha'], 'captcha'],
            [[
                'name',
                'email',
                'text',
            ], 'filter', 'filter' => function ($item) {
                return Html::encode(strip_tags(trim($item)));
            }],
            [['article_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => Article::className(),
                'targetAttribute' => ['article_id' => 'id']
            ],
            [['comment_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => self::className(),
                'targetAttribute' => ['comment_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'article_id' => 'Article ID',
            'comment_id' => 'Comment ID',
            'name' => 'Author',
            'email' => 'Email',
            'text' => 'Comment',
            'rating' => 'Rating',
            'created_at' => 'Created At',
        ];
    }

    public function __toString()
    {
        return nl2br($this->text);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['comment_id' => 'id']);
    }
}
