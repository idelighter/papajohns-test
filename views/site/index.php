<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome</h1>

        <p class="lead">my test application</p>
    </div>

    <div class="body-content">

        <?php if ($message = Yii::$app->session->getFlash('init')): ?>
            <div class="alert alert-info text-center" role="alert"><?= $message ?></div>
        <?php endif; ?>

        <div class="row">
            <div class="col-lg-12">
                <h2>Инструкция по установке приложения</h2>

                <ol>
                 <li>
                        В корневой папке проекта выполнить команду для установк
                        <code>composer update --prefer-dist</code>
                    </li>
                    <li>
                        В файле <code>config/db.php</code> указать настройки подключения в БД:
                        <ul>
                            <li>dbname</li>
                            <li>username</li>
                            <li>password</li>
                        </ul>
                    </li>
                    <li>
                        В корневой папке проекта выполнить команду для создания таблиц<br>
                        <code>./yii migrate/up all --interactive=0</code>
                    </li>
                    <li>
                        Выполнить команду для генерации и вставки в БД статей.
                        <span class="bg-warning "><b>!!!</b> Существующие данные будут удалены <b>!!!</b></span><br>
                        <code>./yii fixture/generate --interactive=0 article && ./yii fixture Article --interactive=0</code>
                    </li>
                    <li>
                        Сгенерить и вставить в БД комменты для случайных статей
                        <span class="bg-warning "><b>!!!</b> Существующие данные будут удалены <b>!!!</b></span><br>
                        <code>./yii fixture/generate --interactive=0 comment && ./yii fixture Comment --interactive=0</code>
                    </li>
                </ol>
            </div>
        </div>

    </div>

</div>
