<?php

namespace tests\unit\fixtures;

class CommentFixture extends \yii\test\ActiveFixture
{
    public $modelClass = 'app\models\Comment';
}
