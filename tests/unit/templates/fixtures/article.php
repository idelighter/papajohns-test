<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */
return [
    'title' => rand(0, 1) ? $faker->sentence() : null,
    'text' => $faker->paragraphs(3, 1),
    'created_at' => join(' ', [$faker->date(), $faker->time()]),
];
