<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax;
use app\models\Article;
use app\models\Comment;
use yii\helpers\Url;
use yii\helpers\StringHelper;
use yii\captcha\Captcha;

/* @var $model Article */
/* @var $formModel Comment */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ($model->title ? Html::encode($model->title) : StringHelper::truncateWords($model, 10));
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <!--Модальное окно с формой добавления коммента.
    При открытии в скрытое поле подставляется ID коммента, который нужно прокомментировать.
    При закрытии окна сбрасываются ошибки валидации.
    При успешном добавлении коммента очищаются текстовые поля.-->
    <div id="commentModal"
         class="modal fade bs-example-modal-lg"
         tabindex="-1"
         role="dialog"
         aria-labelledby="createCommentModalLabel">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button"
                            class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Create Comment</h4>
                </div>
                <?php $form = ActiveForm::begin([
                    'action' => Url::to(['comment/create']),
                ]); ?>
                <div class="modal-body">
                    <?= $form->field($formModel, 'article_id')->hiddenInput()->label(false) ?>
                    <?= $form->field($formModel, 'comment_id')->hiddenInput(['disabled' => 'disabled'])->label(false) ?>
                    <?= $form->field($formModel, 'name')->textInput() ?>
                    <?= $form->field($formModel, 'email')->textInput() ?>
                    <?= $form->field($formModel, 'text')->textarea(['rows' => 6]) ?>
                    <?= $form->field($formModel, 'captcha')->widget(Captcha::className()) ?>
                </div>
                <div class="modal-footer">
                    <button id="commentSave" type="submit" class="btn btn-success">Save</button>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>

    <!--Контейнер отображения статьи.
    Если не задан заголовок, обрезаем статью до 10-ти слов.-->
    <div class="panel panel-success">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-9">
                    <?= $this->title ?>
                </div>
                <div class="col-xs-3">
                    <div class="pull-right">
                        <?= Yii::$app->formatter->asDatetime($model->created_at) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <?= $model ?>
        </div>
    </div>

<?php $pjax = Pjax::begin(); ?>

    <div class="row">
        <div class="col-xs-9">
            <?php if ($dataProvider->count): ?>
                <?= \yii\widgets\LinkPager::widget([
                    'pagination' => $dataProvider->pagination,
                    'firstPageLabel' => "&laquo;&laquo;",
                    'lastPageLabel' => "&raquo;&raquo;",
                ]) ?>
            <?php else: ?>
                <p class="lead" style="margin: 20px 0;">No comments yet</p>
            <?php endif; ?>
        </div>
        <div class="col-xs-3">
            <button type="button"
                    class="btn btn-success pull-right"
                    data-toggle="modal"
                    data-target=".bs-example-modal-lg"
                    style="margin: 20px 0;">Add comment
            </button>
        </div>
    </div>

<?php /** @var Comment $data */
foreach ($dataProvider->models as $data): ?>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <div class="row">
                <div class="col-xs-9">
                    <?= vsprintf('%s (%s)', [
                        Html::encode($data->name),
                        Html::encode($data->email),
                    ]) ?>
                </div>
                <div class="col-xs-3">
                    <div class="pull-right">
                        <?= Yii::$app->formatter->asDatetime($data->created_at) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <?= $data ?>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-xs-12">
                    <button type="button"
                            class="btn btn-primary pull-right"
                            data-toggle="modal"
                            data-comment_id="<?= $data->id ?>"
                            data-target=".bs-example-modal-lg">comment
                    </button>
                </div>
                <div class="col-xs-11 col-xs-offset-1" style="margin-top: 10px;">

                    <!--Под основным комментом выводим ответы других пользователей на него.-->
                    <?php if (count($data->comments)):
                        /** @var Comment $data1 */
                        foreach ($data->comments as $data1): ?>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-9">
                                            <?= vsprintf('%s (%s)', [
                                                Html::encode($data1->name),
                                                Html::encode($data1->email),
                                            ]) ?>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="pull-right">
                                                <?= Yii::$app->formatter->asDatetime($data1->created_at) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <?= $data1 ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<?php Pjax::end(); ?>

<?php $js = <<< JS
$(document).ready(function () {
    var form = $('#{$form->id}');
    var pjaxId = '{$pjax->id}';
    var btn = $('#commentSave');
    var modal = $('#commentModal');
    var commentId = $('#comment-comment_id');
    var yiiData = form.yiiActiveForm('data');

    form.submit(function (event) {
        // По сабмиту формы отправляем данные AJAX-ом, закрываем модальное окно, и обновляем список комментов.
        if (yiiData.validated) {
            $.ajax({
                method: 'POST',
                url: form.attr('action'),
                data: form.serialize(),
                beforeSend: function () {
                    btn.prop('disabled', true);
                },
                complete: function () {
                    btn.prop('disabled', false);
                    yiiData.validated = false;
                }
            })
            .done(function (data, textStatus) {
                if (data.status == textStatus) { // success
                    $.pjax.reload('#' + pjaxId);
                    $('#comment-name, #comment-email, #comment-text, #comment-captcha', form).val('');
                    $('#comment-captcha-image').click();
                }
            })
            .always(function () {
                modal.modal('hide');
            });
        }

        event.preventDefault();
    });

    modal.on('hide.bs.modal', function (e) {
        form.yiiActiveForm('resetForm');
        commentId.prop('disabled', true);
        commentId.val('');
    });
    modal.on('show.bs.modal', function (e) {
        var comment_id = $(e.relatedTarget).data('comment_id');

        if (comment_id) {
            commentId.prop('disabled', false);
            commentId.val(comment_id);
        }
    });

    // Поиск и подмена "на лету" URL-ов на кликабельные ссылки
    var regexp = /http[s]?:\/\/\S+/g;
    var extractLinks = function () {
        $('.panel > .panel-body').each(function () {

            var formattedHtml = $(this).html().replace(regexp, function (match) {
                var div = $('<div>');
                var a = $('<a>')
                    .attr('href', match)
                    .attr('target', '_blank')
                    .text(match)
                    .appendTo(div);

                return div.html();
            });

            $(this).html(formattedHtml);
        });
    };

    $(document).on('pjax:complete', function() {
        extractLinks();
    });

    extractLinks();
});
JS;

$this->registerJs($js);
