<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */
return [
    'article_id' => rand(1, 10),
    'name' => $faker->userName,
    'email' => $faker->email,
    'text' => $faker->text(),
    'created_at' => join(' ', [$faker->date(), $faker->time()]),
];
