<?php

namespace tests\unit\fixtures;

class ArticleFixture extends \yii\test\ActiveFixture
{
    public $modelClass = 'app\models\Article';
}
